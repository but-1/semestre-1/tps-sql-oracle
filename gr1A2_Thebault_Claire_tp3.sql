/*
TD & TP3 - Test de contraintes de cr�ation de tables
Insertion , Suooression et Mis � jour de donn�es
Nom : Th�bault
Pr�nom : Claire
Groupe : A2
*/

/* 
Enseignant( nomEns (1), prenomEns, adresse, statut )  
Cycle( num (1), enseignantResponsable = @Enseignant.nomEns (UQ)(NN) )
Cours( nomCours (1), volumeH, lEnseignant = @Enseignant.nomEns (NN), leCycle = @Cycle.num (NN) ) 
Requiert( [ cours = @Cours.nomCours, coursRequis = @Cours.nomCours ](1) )
*/

-- Question 1 : Ex�cuter le script de cr�ation de tables

DROP TABLE Requiert ;
DROP TABLE Cours ;
DROP TABLE Cycle ; 
DROP TABLE Enseignant ;

CREATE TABLE Enseignant
	(
	nomEns VARCHAR2(20)
		CONSTRAINT pk_Enseignant PRIMARY KEY,	
	prenomEns VARCHAR2(20),
	adresse VARCHAR2(40),
	statut VARCHAR2(20)
	)
;

CREATE TABLE Cycle
	(
	num NUMBER
		CONSTRAINT pk_Cycle PRIMARY KEY,	
	enseignantResponsable VARCHAR2(20) 
		CONSTRAINT fk_Cycle_Enseignant REFERENCES Enseignant(nomEns)
		CONSTRAINT uq_enseignantResponsable UNIQUE
		CONSTRAINT nn_enseignantResponsable NOT NULL
	)
;

CREATE TABLE Cours
	(
	nomCours VARCHAR2(20)
		CONSTRAINT pk_Cours PRIMARY KEY,	
	volumeH NUMBER,
	lEnseignant VARCHAR2(20)
		CONSTRAINT fk_Cours_Enseignant REFERENCES Enseignant(nomEns)
		CONSTRAINT nn_lEnseignant NOT NULL,	
	leCycle NUMBER
		CONSTRAINT fk_Cours_Cycle REFERENCES Cycle(num)	
		CONSTRAINT nn_leCycle NOT NULL
	)
;

CREATE TABLE Requiert
	(
	cours VARCHAR2(20)
		CONSTRAINT fk_Requiert_Cours REFERENCES Cours(nomCours), 
	coursRequis VARCHAR2(20)
		CONSTRAINT fk_Requiert_CoursRequis REFERENCES Cours(nomCours), 
	CONSTRAINT pk_Requiert PRIMARY KEY (cours, coursRequis)
	)
;

-- Question 2 : Script d'insertion
-- 5 enseignants
INSERT INTO Enseignant VALUES('Pham','Minh Tan', NULL,'enseignant chercheur');
INSERT INTO Enseignant (nomEns) VALUES('Kamp');
INSERT INTO Enseignant VALUES('Naert', 'Lucie', NULL, 'enseignant');
INSERT INTO Enseignant (nomEns, prenomEns) VALUES('Fleurquin', 'Regis');
INSERT INTO Enseignant (nomEns) VALUES('Tonin');

-- 3 cycles
INSERT INTO Cycle VALUES (1, 'Naert');
INSERT INTO Cycle VALUES (2, 'Kamp');
INSERT INTO Cycle VALUES (3, 'Fleurquin');

-- 5 cours
INSERT INTO Cours VALUES ('Math�matiques', 20, 'Fleurquin', 1);
INSERT INTO Cours VALUES ('SQL', 25, 'Pham', 1);
INSERT INTO Cours VALUES ('Java', 20, 'Naert', 1);
INSERT INTO Cours VALUES ('Anglais', 15, 'Tonin', 1);
INSERT INTO Cours VALUES ('D�veloppement Web', 10, 'Kamp' , 1);

-- 2 requierts 
INSERT INTO Requiert VALUES ('Math�matiques', 'SQL');
INSERT INTO Requiert VALUES ('Java', 'D�veloppement Web');


-- Question 3 : Pour r�-ex�cuter le script d'insertion, il faut d'abord vider les donn�es avec DELETE FROM (dans l'ordre)

DELETE FROM Requiert;
DELETE FROM Cours;
DELETE FROM Cycle;
DELETE FROM Enseignant;

-- Question 4: Test de contraintes
-- a) l'existence de la cl�  primaire de la table Enseignant
INSERT INTO Enseignant(nomEns) VALUES(NULL);
/*
Erreur commen�ant � la ligne: 102 de la commande -
INSERT INTO Enseignant(nomEns) VALUES(NULL)
Rapport d'erreur -
ORA-01400: cannot insert NULL into ("CLAIRE"."ENSEIGNANT"."NOMENS")
*/



-- b) l'unici� de la cl� primaire de la table Enseignant
INSERT INTO Enseignant(nomEns) VALUES('Naert');
INSERT INTO Enseignant VALUES('Naert', 'Lucie', NULL, 'enseignant');
/*
Erreur commen�ant � la ligne: 114 de la commande -
INSERT INTO Enseignant VALUES('Naert', 'Lucie', NULL, 'enseignant')
Rapport d'erreur -
ORA-00001: unique constraint (CLAIRE.PK_ENSEIGNANT) violated
*/

-- c) l'existence et l'unicit� de la cl� �trang�re dans la table  Cycle
-- existence
INSERT INTO Cycle VALUES(1, NULL);
/*
Erreur commen�ant � la ligne: 124 de la commande -
INSERT INTO Cycle VALUES(1, NULL)
Rapport d'erreur -
ORA-01400: cannot insert NULL into ("CLAIRE"."CYCLE"."ENSEIGNANTRESPONSABLE")
*/

-- unicit�
INSERT INTO Cycle VALUES(1, 'Naert');
INSERT INTO Cycle VALUES(2, 'Naert');
/*
Erreur commen�ant � la ligne: 128 de la commande -
INSERT INTO Cycle VALUES(2, 'Naert')
Rapport d'erreur -
ORA-00001: unique constraint (CLAIRE.UQ_ENSEIGNANTRESPONSABLE) violated
*/

DELETE FROM Requiert;
DELETE FROM Cours;
DELETE FROM Cycle;
DELETE FROM Enseignant;


-- Question 5:
--a) ajout d'une contrainte volumeH > 0
ALTER TABLE Cours ADD CONSTRAINT ck_volumeH CHECK (volumeH>0);

--b)tester la contrainte
INSERT INTO Cours VALUES ('Math�matiques', -1, 'Fleurquin', 1);
/*
Erreur commen�ant � la ligne: 153 de la commande -
INSERT INTO Cours VALUES ('Math�matiques', -1, 'Fleurquin', 1)
Rapport d'erreur -
ORA-02290: check constraint (CLAIRE.CK_VOLUMEH) violated
*/

--c)enlever la contrainte
ALTER TABLE Cours DROP CONSTRAINT ck_volumeH;


--Question 6: 
ALTER TABLE Enseignant ADD ageEns VARCHAR2(3)
    CONSTRAINT nn_ageEns NOT NULL;
ALTER TABLE Enseignant DROP COLUMN ageEns;

-- Question 7:
INSERT INTO Enseignant VALUES('Naert', 'Lucie', NULL, 'enseignant');
INSERT INTO Enseignant (nomEns, prenomEns) VALUES('Fleurquin', 'Regis');
INSERT INTO Cycle VALUES (1, 'Naert');
INSERT INTO Cours VALUES ('Math�matiques', 20, 'Naert', 1);

-- Mettre � jour avec UPDATE
UPDATE Cours 
    SET lEnseignant = 'Fleurquin'
    WHERE nomCours = 'Math�matiques';

-- Enlever avec DELETE FROM
INSERT INTO Enseignant VALUES ('Lemaitre', NULL, NULL, 'blabla');
DELETE FROM Enseignant
WHERE statut = 'blabla';