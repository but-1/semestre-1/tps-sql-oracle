/*
TP2 SQL : Cr�ation de table
Claire
Th�bault
GR1A2
*/

/*
--Exercice 1 :

Enseignant(nomEns(1), prenomEns, adresse, statut)
Cycle(num(1), enseignantResponsable = @Enseignant.nomEns(UQ)(NN) )
Cours(nomCours(1), volumeH, lEnseignant=@Enseignant.nomEns(NN), leCycle=@Cycle.num(NN) )
Requiert([cours=@Cours.nomCours, coursRequis=@Cours.nomCours](1) )

��Avec :
- Les 2 attributs num et volumeH sont de type NUMBER(precision,scale)
- Les autres attributs sont des cha?nes de caract�res (type VARCHAR2(length) en SQL ORACLE)
*/

-- Question 1 :
/*
Ordre de cr�ation des tables :
1 - Enseignant
2 - Cycle 
3 - Cours
4 - Requiert

Pour d�truire on proc�de dans l'ordre inverse
*/

-- Question 2 : script de cr�ation

CREATE TABLE Enseignant
    (
    nomEnse VARCHAR2(30)
        CONSTRAINT pk_Enseignant PRIMARY KEY,
    prenomEns VARCHAR2(30),
    adresse VARCHAR2(30),
    statut VARCHAR2(30)
    )
;

CREATE TABLE Cycle
    (
    num NUMBER
        CONSTRAINT pk_Cycle PRIMARY KEY,

    enseignantResponsable VARCHAR2(20)
        CONSTRAINT fk_Cycle_Enseignant REFERENCES Enseignant(nomEns)
        CONSTRAINT nn_enseignantResponsable NOT NULL
        CONSTRAINT uq_enseignantResponsable UNIQUE
    )
;

CREATE TABLE Cours
    (
    nomCours VARCHAR2(20)
        CONSTRAINT pk_nomCours PRIMARY KEY,

    volumeH NUMBER,
    lEnseignant VARCHAR2(20)
        CONSTRAINT fk_Cours_Enseignant REFERENCES Enseignant(nomEns)
        CONSTRAINT nn_lEnseignant NOT NULL,

    leCycle NUMBER
        CONSTRAINT fk_Cours_Cycle REFERENCES Cycle(num)
        CONSTRAINT nn_leCycle NOT NULL
    )
;

CREATE TABLE Requiert
    (
    cours VARCHAR2(20)
        CONSTRAINT fk_Requiert_Cours REFERENCES Cours(nomCours),

    coursRequis VARCHAR2(20)
        CONSTRAINT fk_Requiert_CoursRequis REFERENCES Cours(nomCours),

    CONSTRAINT pk_Requiert PRIMARY KEY (cours, coursRequis)
    )
;

-- Question 3 :

DROP TABLE Requiert;
DROP TABLE Cours;
DROP TABLE Cycle;
DROP TABLE Enseignant;

CREATE TABLE Enseignant
    (
    nomEns VARCHAR2(20)
        CONSTRAINT pk_Enseignant PRIMARY KEY,

    prenomEns VARCHAR2(20),
    adresse VARCHAR2(20),
    statut VARCHAR2(20)
    )
;

CREATE TABLE Cycle
    (
    num NUMBER
        CONSTRAINT pk_Cycle PRIMARY KEY,

    enseignantResponsable VARCHAR2(20)
        CONSTRAINT fk_Cycle_Enseignant REFERENCES Enseignant(nomEns)
        CONSTRAINT nn_enseignantResponsable NOT NULL
        CONSTRAINT uq_enseignantResponsable UNIQUE
    )
;

CREATE TABLE Cours
    (
    nomCours VARCHAR2(20)
        CONSTRAINT pk_nomCours PRIMARY KEY,

    volumeH NUMBER,
    lEnseignant VARCHAR2(20)
        CONSTRAINT fk_Cours_Enseignant REFERENCES Enseignant(nomEns)
        CONSTRAINT nn_lEnseignant NOT NULL,

    leCycle NUMBER
        CONSTRAINT fk_Cours_Cycle REFERENCES Cycle(num)
        CONSTRAINT nn_leCycle NOT NULL
    )
;

CREATE TABLE Requiert
    (
    cours VARCHAR2(20)
        CONSTRAINT fk_Requiert_Cours REFERENCES Cours(nomCours),

    coursRequis VARCHAR2(20)
        CONSTRAINT fk_Requiert_CoursRequis REFERENCES Cours(nomCours),

    CONSTRAINT pk_Requiert PRIMARY KEY (cours, coursRequis)
    )
;
-- Question 3 :
DROP TABLE Requiert;
DROP TABLE Cours;
DROP TABLE Cycle;
DROP TABLE Enseignant;


-- Exercice 2
/*
Proprietaire(idProprietaire(1), nomProprietaire(NN), prenomProprietaire(NN), emailProprietaire(UQ)(NN))
Emplacement(idEmplacement(1), longueurEmplacement(NN), coutJournalier(NN))
Bateau(idBateau(1), nomBateau, longueurBateau(NN), leProprietaire = @Proprietaire.idProprietaire(NN), leStationnement = @Emplacement.idEmplacement(NN) )
Reservation(idReservation(1), dateDebut(NN), dateFin(NN), leBateau = @Bateau.idBateau(NN), lEmplacement = @Emplacement.idEmplacement(NN))

��Avec :
- Les attributs d�identifiant et de taille sont de type NUMBER - Les autres attributs sont de type VARCHAR2
- Les attributs dateDebut et dateFin sont de type DATE

*/

-- Question 4 : script de cr�ation de tables de la base de donn�es Bateau 

DROP TABLE Reservation;
DROP TABLE Bateau;
DROP TABLE Emplacement;
DROP TABLE Proprietaire;


CREATE TABLE Proprietaire
    (
    idProprietaire NUMBER
        CONSTRAINT pk_Proprietaire PRIMARY KEY,

    nomProprietaire VARCHAR2(20)
        CONSTRAINT nn_nomProprietaire NOT NULL,

    prenomProprietaire VARCHAR2(20)
        CONSTRAINT nn_prenomProprietaire NOT NULL,

    emailProprietaire VARCHAR2(20)
        CONSTRAINT nn_emailProprietaire NOT NULL
        CONSTRAINT uq_emailProprietaire UNIQUE,
    )
;

CREATE TABLE Emplacement
    (
    idEmplacement NUMBER
        CONSTRAINT pk_Emplcement PRIMARY KEY,
    
    longueurEmplacement NUMBER
        CONSTRAINT nn_longueurEmplacement NOT NULL,

    coutJournalier NUMBER
        CONSTRAINT nn_coutJournalier NOT NULL
    )
;

CREATE TABLE Bateau
    (
    idBateau NUMBER
        CONSTRAINT pk_Bateau PRIMARY KEY,

    nomBateau VARCHAR2(20),

    longueurBateau NUMBER
        CONSTRAINT nn_longueurBateau NOT NULL,

    leProprietaire NUMBER
        CONSTRAINT fk_Bateau_Proprietaire REFERENCES Proprietaire(idProprietaire),

    leStationnement NUMBER
        CONSTRAINT fk_Bateau_Emplacement REFERENCES Emplacement(idEmplacement)
    )
;

CREATE TABLE Reservation
    (
    idReservation NUMBER
        CONSTRAINT pk_Reservation PRIMARY KEY,

    dateDebut DATE
        CONSTRAINT nn_dateDebut NOT NULL,

    dateFin DATE
        CONSTRAINT nn_dateFin NOT NULL,

    leBateau NUMBER
        CONSTRAINT fk_Reservation_Bateau REFERENCES Bateau(idBateau)
        CONSTRAINT nn_leBateau NOT NULL,

    lEmplacement NUMBER
        CONSTRAINT fk_Reservation_Emplacement REFERENCES Emplacement(idEmplacement)
        CONSTRAINT nn_lEmplacement NOT NULL
    )
;

-- Question 5 :

DROP TABLE Reservation;
DROP TABLE Bateau;
DROP TABLE Emplacement;
DROP TABLE Proprietaire;


CREATE TABLE Proprietaire
    (
    idProprietaire NUMBER
        CONSTRAINT pk_Proprietaire PRIMARY KEY,

    nomProprietaire VARCHAR2(20)
        CONSTRAINT nn_nomProprietaire NOT NULL,

    prenomProprietaire VARCHAR2(20)
        CONSTRAINT nn_prenomProprietaire NOT NULL,

    emailProprietaire VARCHAR2(20)
        CONSTRAINT nn_emailProprietaire NOT NULL
        CONSTRAINT uq_emailProprietaire UNIQUE
        CONSTRAINT ck_emailProprietaire CHECK(emailProprietaire LIKE '%_@%_.%_')
    )
;

CREATE TABLE Emplacement
    (
    idEmplacement NUMBER
        CONSTRAINT pk_Emplcement PRIMARY KEY,
    
    longueurEmplacement NUMBER
        CONSTRAINT nn_longueurEmplacement NOT NULL,

    coutJournalier NUMBER
        CONSTRAINT nn_coutJournalier NOT NULL
    )
;

CREATE TABLE Bateau
    (
    idBateau NUMBER
        CONSTRAINT pk_Bateau PRIMARY KEY,

    nomBateau VARCHAR2(20),

    longueurBateau NUMBER
        CONSTRAINT nn_longueurBateau NOT NULL
        CONSTRAINT ck_longueurBateau CHECK(longueurBateau <= 20),

    leProprietaire NUMBER
        CONSTRAINT fk_Bateau_Proprietaire REFERENCES Proprietaire(idProprietaire),

    leStationnement NUMBER
        CONSTRAINT fk_Bateau_Emplacement REFERENCES Emplacement(idEmplacement)
    )
;

CREATE TABLE Reservation
    (
    idReservation NUMBER
        CONSTRAINT pk_Reservation PRIMARY KEY,

    dateDebut DATE
        CONSTRAINT nn_dateDebut NOT NULL,

    dateFin DATE
        CONSTRAINT nn_dateFin NOT NULL,
        CONSTRAINT ck_dateFin CHECK(dateDebut < dateFin),

    leBateau NUMBER
        CONSTRAINT fk_Reservation_Bateau REFERENCES Bateau(idBateau)
        CONSTRAINT nn_leBateau NOT NULL,

    lEmplacement NUMBER
        CONSTRAINT fk_Reservation_Emplacement REFERENCES Emplacement(idEmplacement)
        CONSTRAINT nn_lEmplacement NOT NULL
    )
;

-- Question 6 : 
/*
Contrainte : la longueur du bateau doit �tre su^p�rieur � 0
ALTER TABLE Bateau ADD CONSTRAINT ck_long_BateauSup CHECK (longueurBateau > 0);

Contrainte : la longueur du bateau doit �tre su^p�rieur � 0
ALTER TABLE Emplacement ADD CONSTRAINT ck_longEmplacementSup CHECK (longueurBateau > 0);
*/




    
