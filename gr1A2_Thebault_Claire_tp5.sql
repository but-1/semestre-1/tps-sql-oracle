/*
TP5 SQL : 
Claire
Th�bault
GR1A2
*/



CREATE TABLE EnseignantInfo(
		noEns NUMBER(4)
			CONSTRAINT pk_EnseignantInfo PRIMARY KEY,
		nomEns VARCHAR2(20),
		prenomEns VARCHAR2(20)
	)
;

INSERT INTO EnseignantInfo VALUES(1,'KAmP','Jean Francois');
INSERT INTO EnseignantInfo VALUES(2,'BEAuDONT','Pascal');
INSERT INTO EnseignantInfo VALUES(3,'GODIN','Thibault');
INSERT INTO EnseignantInfo VALUES(4,'KhayaTa','Mohammed');
INSERT INTO EnseignantInfo VALUES(5,'TONIN','Philippe');
INSERT INTO EnseignantInfo VALUES(6,'ROIRAND','Xavier');
INSERT INTO EnseignantInfo VALUES(7,'LE SOmMER','Nicolas');
INSERT INTO EnseignantInfo VALUES(8,'MErCIOL','Francois');
INSERT INTO EnseignantInfo VALUES(9,'LE Lain','Matthieu');
INSERT INTO EnseignantInfo VALUES(10,'LEFEVRE','Sebastien');
INSERT INTO EnseignantInfo VALUES(11,'BORNE','Isabelle');
INSERT INTO EnseignantInfo VALUES(12,'PHAM','Minh Tan');
INSERT INTO EnseignantInfo VALUES(13,'FLEURQUIN','Regis');
INSERT INTO EnseignantInfo VALUES(14,'NAeRT','Lucie');
INSERT INTO EnseignantInfo VALUES(15,'Belmouhcine','Abdelbadie');
INSERT INTO EnseignantInfo VALUES(16,'LeMaitre','Elodie');
INSERT INTO EnseignantInfo VALUES(17,'TUFFIGOT','Helene');
INSERT INTO EnseignantInfo VALUES(18,'Joucla','Philippe');
INSERT INTO EnseignantInfo VALUES(19,'Kerbellec','Goulven');
INSERT INTO EnseignantInfo VALUES(20,'Raut','Sophie');


CREATE TABLE EtudiantInfo(
		noEtu NUMBER(4)
			CONSTRAINT pk_EtudiantInfo PRIMARY KEY,
		nomEtu VARCHAR2(40),
		prenom VARCHAR2(30),
		groupe VARCHAR2(5)
	)
;

INSERT INTO EtudiantInfo VALUES(1,'ALEXANDRE','Nathan','B');
INSERT INTO EtudiantInfo VALUES(2,'ANNEIX','Am�lie','B');
INSERT INTO EtudiantInfo VALUES(3,'ARANDEL','Cyprien','D');
INSERT INTO EtudiantInfo VALUES(4,'BARATHON','Polig','B');
INSERT INTO EtudiantInfo VALUES(5,'BARIOU','Melvyn','A');
INSERT INTO EtudiantInfo VALUES(6,'BELMOKHTAR','Hocin','B');
INSERT INTO EtudiantInfo VALUES(7,'BELOUAHRANI KEBIR HOCEINI','Ilias','D');
INSERT INTO EtudiantInfo VALUES(8,'BELTRAME','Ma�l','A');
INSERT INTO EtudiantInfo VALUES(9,'BERNARD-GRIFFITHS','Samuel','A');
INSERT INTO EtudiantInfo VALUES(10,'BILLARD','Louis','B');
INSERT INTO EtudiantInfo VALUES(11,'BURBAN','L�o','C');
INSERT INTO EtudiantInfo VALUES(12,'CABARET','Louann','D');
INSERT INTO EtudiantInfo VALUES(13,'CACQUEVEL','Robinson','C');
INSERT INTO EtudiantInfo VALUES(14,'CAMPION','Brayan','B');
INSERT INTO EtudiantInfo VALUES(15,'CAROMEL','Merlin','B');
INSERT INTO EtudiantInfo VALUES(16,'CARR�','Louan','B');
INSERT INTO EtudiantInfo VALUES(17,'CELINI','Alvin','D');
INSERT INTO EtudiantInfo VALUES(18,'CESMAT-BELLIARD','Alexis','B');
INSERT INTO EtudiantInfo VALUES(19,'CHAUVELIN','Thomas','A');
INSERT INTO EtudiantInfo VALUES(20,'CHAZEL','Matteo','A');
INSERT INTO EtudiantInfo VALUES(21,'COIGNARD','Ma�l','C');
INSERT INTO EtudiantInfo VALUES(22,'COUTANT','Jade','C');
INSERT INTO EtudiantInfo VALUES(23,'CRUNCHANT','Manon','D');
INSERT INTO EtudiantInfo VALUES(24,'DESBROUSSES','Victor','A');
INSERT INTO EtudiantInfo VALUES(25,'DIONNE','Cl�ment','D');
INSERT INTO EtudiantInfo VALUES(26,'DITTBERNER','Emmeran','A');
INSERT INTO EtudiantInfo VALUES(27,'DJERBI','Ilies','A');
INSERT INTO EtudiantInfo VALUES(28,'DOLIVET','Milo','C');
INSERT INTO EtudiantInfo VALUES(29,'DUBOIS','Ryann','A');
INSERT INTO EtudiantInfo VALUES(30,'EL HOMADI--LEVEQUE','Ayette','C');
INSERT INTO EtudiantInfo VALUES(31,'EMERAUD','Jean-Louis','A');
INSERT INTO EtudiantInfo VALUES(32,'ESKHADJIEV','Abdul-Malik','D');
INSERT INTO EtudiantInfo VALUES(33,'FERRON','Sarah','A');
INSERT INTO EtudiantInfo VALUES(34,'FRANCERIES','Iban','D');
INSERT INTO EtudiantInfo VALUES(35,'GARCIA','Ella�s','A');
INSERT INTO EtudiantInfo VALUES(36,'GAUFFENY','PAuL','A');
INSERT INTO EtudiantInfo VALUES(37,'GIRE','Aliaume','A');
INSERT INTO EtudiantInfo VALUES(38,'GODEFROY','Marius','D');
INSERT INTO EtudiantInfo VALUES(39,'GOMEZ--JEGO','Inaki','A');
INSERT INTO EtudiantInfo VALUES(40,'GUHENEUF-LE BREC','NaTHAN','A');
INSERT INTO EtudiantInfo VALUES(41,'GUILLEMAUD','Glenn','B');
INSERT INTO EtudiantInfo VALUES(42,'GUILLOUET','Isma�l','C');
INSERT INTO EtudiantInfo VALUES(43,'GUINOT','William','D');
INSERT INTO EtudiantInfo VALUES(44,'G�NES','Omer','B');
INSERT INTO EtudiantInfo VALUES(45,'HAMON','Louis','C');
INSERT INTO EtudiantInfo VALUES(46,'HEULAN','Malki','C');
INSERT INTO EtudiantInfo VALUES(47,'HUET CHAPUIS','Martin','C');
INSERT INTO EtudiantInfo VALUES(48,'HUYNH-BA','Baptiste','C');
INSERT INTO EtudiantInfo VALUES(49,'ISMAIL OMAR','Tasnim','B');
INSERT INTO EtudiantInfo VALUES(50,'JAN','Oc�ane','C');
INSERT INTO EtudiantInfo VALUES(51,'JULES--VACHET','Matth�o','A');
INSERT INTO EtudiantInfo VALUES(52,'KERVEILLANT','Elie','D');
INSERT INTO EtudiantInfo VALUES(53,'KOLB','Mattis','D');
INSERT INTO EtudiantInfo VALUES(54,'LABB�','Pierre-Louis','D');
INSERT INTO EtudiantInfo VALUES(55,'LABHINI','Lucas','C');
INSERT INTO EtudiantInfo VALUES(56,'LACOUR','Alexandre','C');
INSERT INTO EtudiantInfo VALUES(57,'LE BRECH','Camille','D');
INSERT INTO EtudiantInfo VALUES(58,'LE MAGUER','NATHAN','A');
INSERT INTO EtudiantInfo VALUES(59,'LE PROVOST','Louen','D');
INSERT INTO EtudiantInfo VALUES(60,'LEF�VRE','Nathan','C');
INSERT INTO EtudiantInfo VALUES(61,'LEFRANC','Th�au','B');
INSERT INTO EtudiantInfo VALUES(62,'LEGEAY','Quentin','B');
INSERT INTO EtudiantInfo VALUES(63,'LEGRAND','Gabin','B');
INSERT INTO EtudiantInfo VALUES(64,'LESAUVAGE','L�andre','B');
INSERT INTO EtudiantInfo VALUES(65,'LOISANCE--PINEL','Melen','D');
INSERT INTO EtudiantInfo VALUES(66,'LORCY','Ilan','C');
INSERT INTO EtudiantInfo VALUES(67,'MAREC','Amaury','B');
INSERT INTO EtudiantInfo VALUES(68,'MARIOT','Est�ban','B');
INSERT INTO EtudiantInfo VALUES(69,'MARQUE','Dylan','B');
INSERT INTO EtudiantInfo VALUES(70,'MELLAH','Rayanne','C');
INSERT INTO EtudiantInfo VALUES(71,'MICHELET','Eliaz','D');
INSERT INTO EtudiantInfo VALUES(72,'MIGNIER--NHEAN','Aymeric','C');
INSERT INTO EtudiantInfo VALUES(73,'MORHAN','William','B');
INSERT INTO EtudiantInfo VALUES(74,'MORNET','Quentin','C');
INSERT INTO EtudiantInfo VALUES(75,'MOUELLO','Samuel','B');
INSERT INTO EtudiantInfo VALUES(76,'NACHIN','Alexia','D');
INSERT INTO EtudiantInfo VALUES(77,'NATIVELLE','Elliot','C');
INSERT INTO EtudiantInfo VALUES(78,'NDUNGINI','J�r�my-Yoan','B');
INSERT INTO EtudiantInfo VALUES(79,'NOUVION','Mateo','B');
INSERT INTO EtudiantInfo VALUES(80,'PARCOLLET','No�','C');
INSERT INTO EtudiantInfo VALUES(81,'PATINEC','Francois','A');
INSERT INTO EtudiantInfo VALUES(82,'P�RON','Romain','B');
INSERT INTO EtudiantInfo VALUES(83,'PICARD','Benjamin','A');
INSERT INTO EtudiantInfo VALUES(84,'PIVRON','Jonas','A');
INSERT INTO EtudiantInfo VALUES(85,'PONDAVEN','Thibault','D');
INSERT INTO EtudiantInfo VALUES(86,'REBOURS','Maxence','C');
INSERT INTO EtudiantInfo VALUES(87,'RIVALLAND','Kyllian','A');
INSERT INTO EtudiantInfo VALUES(88,'RODRIGUES','Matt�o','B');
INSERT INTO EtudiantInfo VALUES(89,'ROUSSEL','Paul','C');
INSERT INTO EtudiantInfo VALUES(90,'SAUNDERS','Benjamin','C');
INSERT INTO EtudiantInfo VALUES(91,'SCHELL','Yanis','A');
INSERT INTO EtudiantInfo VALUES(92,'SITKO','Galian','A');
INSERT INTO EtudiantInfo VALUES(93,'SZCZEPANSKI','Serge','D');
INSERT INTO EtudiantInfo VALUES(94,'TH�BAULT','Claire','A');
INSERT INTO EtudiantInfo VALUES(95,'TR�VIAN','Benjamin','D');
INSERT INTO EtudiantInfo VALUES(96,'TROUSSELET','Kylian','A');
INSERT INTO EtudiantInfo VALUES(97,'TUCAT','Mathieu','A');
INSERT INTO EtudiantInfo VALUES(98,'VARRIER','Mathis','C');
INSERT INTO EtudiantInfo VALUES(99,'VIDAL','Titouan','D');
INSERT INTO EtudiantInfo VALUES(100,'VION','Iann','D');
INSERT INTO EtudiantInfo VALUES(101,'VRIGNAUD','pAul','B');
INSERT INTO EtudiantInfo VALUES(102,'ZENSEN--DA SILVA','Gabriel','D');
INSERT INTO EtudiantInfo VALUES(103,'ZIEGELMEYER','Eliot','D');

--Exercice 2 :
/* Question 5 : Afficher tous les tuples des tables EnseignantInfo et EtudiantInfo pour visualiser des donnees.
Ecrivez le schema relationnel des deux tables.*/

SELECT * 
FROM EtudiantInfo;
-- 103 rows selected.

SELECT *
FROM EnseignantInfo;
-- 20 rows selected

--Sch�ma relationnel:
/*
EtudiantInfo(noEtu(1), nomEtu, prenom, groupe)
EnseignantInfo(noEns(1), nomEns, prenomEns)
*/

/* Question 6: Quels sont les prenoms des �tudiants ? Effectuer la requete avec et sans utilisant UPPER. 
Pourquoi trouve-t-on deux r�sultats diff�rents ?
*/
-- On trouve des r�sultats diff�rents car certains noms dans la liste sont �crit sans majuscule au d�but ou bien avec une majuscule 
--au milieu du mot utiliser UPPER permet de comparer les noms sans tenir compte des majuscules et minuscules.

--Al. Rel. EtudiantInfo[prenom]
SELECT DISTINCT prenom
FROM EtudiantInfo;
/* 95 tuples
PRENOM
Ilias
Samuel
Brayan
Matteo
Iban
Marius
Isma�l
Louen
Th�au
Melen
Amaury
...
*/

SELECT DISTINCT UPPER (prenom)
FROM EtudiantInfo;
/* 91 tuples
UPPER(PRENOM)                 
AM�LIE
BRAYAN
MERLIN
ALVIN
MATTEO
...
*/

-- Question 7 : Quels sont les etudiants prenommes �Louis� (afficher toutes les informations) ?
--Al. Rel. EtudiantInfo{prenom = 'Louis'}

SELECT *
FROM EtudiantInfo
WHERE UPPER(prenom) = 'LOUIS';
/*
     NOETU NOMETU                                   PRENOM                         GROUP
---------- ---------------------------------------- ------------------------------ -----
        10 BILLARD                                  Louis                          B    
        45 HAMON                                    Louis                          C
*/

-- Question 8 : Quels sont les �etudiants prenommes �Victor� ou �Paul� ?
--Al. Rel. EtudiantInfo{prenom = 'VICTOR' or prenom ='Paul'}

SELECT *
FROM EtudiantInfo
WHERE UPPER(prenom) = 'VICTOR' OR UPPER(prenom) = 'PAUL';

/*
     NOETU NOMETU                                   PRENOM                         GROUP
---------- ---------------------------------------- ------------------------------ -----
        24 DESBROUSSES                              Victor                         A    
        36 GAUFFENY                                 PAuL                           A    
        89 ROUSSEL                                  Paul                           C    
       101 VRIGNAUD                                 pAul                           B    
*/

--Question 9 : Quels sont les �etudiants dans un autre groupe TD qui ont votre prenom ?
--Al. Rel. EtudiantInfo{UPPER(prenom) = 'CLAIRE' AND groupe != 'A'}
SELECT *
FROM EtudiantInfo
WHERE UPPER(prenom) = 'CLAIRE'  AND groupe != 'A';
/*
aucune ligne s�lectionn�e
*/

--Question 10 :  Quels sont les noms port�es par les �etudiants ou les enseignants (avec le renommage des colonnes utilisant AS) ?
--Al. Rel. EtudiantInfo[nomEtu] UNION  EnseignantInfo[nomEns]
SELECT nomEtu AS nom
FROM EtudiantInfo 
UNION 
SELECT nomEns AS nom
FROM EnseignantInfo ;
/*
123 lignes s�lectionn�es.
NOM                                     
----------------------------------------
ALEXANDRE
ANNEIX
ARANDEL
BARATHON
...
*/

--Question 11 : Quels sont les prenoms portes a la fois par des etudiants et des enseignants (avec le renommage) ?
--Al. Rel. EtudiantInfo[prenom] INTERSECT EnseignantInfo[prenom]
SELECT prenom AS prenom
FROM EtudiantInfo 
INTERSECT
SELECT prenomEns AS prenom
FROM EnseignantInfo ;
/*
PRENOM                        
------------------------------
Francois
Thibault
*/

-- Question 12 : Quels sont les prenoms portes par des enseignants, mais pas par des etudiants ?
--Al. Rel. EnseignantInfo[prenomEns]\EtudiantInfo[prenom]
SELECT prenomEns AS prenom
FROM EnseignantInfo
MINUS
SELECT prenom AS prenom
FROM EtudiantInfo;
/*
17 lignes s�lectionn�es. 
PRENOM                        
------------------------------
Abdelbadie
Elodie
Goulven
Helene
...
*/

--Question 13 : Quels sont les prenoms des enseignants ordonnes par ordre alphabetique descendant ?
--Al. Rel. EnseignantInfo{prenomEns ORDER BY DESC}[prenomEns]
SELECT DISTINCT prenomEns AS prenom
FROM EnseignantInfo
ORDER BY prenomEns DESC;
/*
19 lignes s�lectionn�es.
PRENOM                        
------------------------------
Abdelbadie
Elodie
Goulven
Helene
...
*/

--Question 14 : Quels sont les noms et prenoms des etudiants dont le nom commence par �A� et contient une deuxieme fois la lettre �A� ?
--Al. Rel. EtudiantInfo{UPPER(nomEtu) LIKE 'A%A%'}[nomEtu, prenom] 
SELECT nomEtu, prenom
FROM EtudiantInfo 
WHERE nomEtu LIKE 'A%A%' ;

/*
NOMETU                                   PRENOM                        
---------------------------------------- ------------------------------
ALEXANDRE                                Nathan                        
ARANDEL                                  Cyprien                       
*/

--Question 15 : Quels sont les numeros et noms des etudiants ordonnes par ordre alphabetique des noms et prenoms et dont le nom se termine par �A� ?
-- Al. Rel. EtudiantInfo{UPPER(nomEtu)='%A'}[noEtu, nomEtu]
SELECT noEtu, nomEtu
    FROM (SELECT *
    FROM EtudiantInfo
    ORDER BY nomEtu )
WHERE nomEtu LIKE '%A';

/*
     NOETU NOMETU                                  
---------- ----------------------------------------
        35 GARCIA                                  
        48 HUYNH-BA                                
       102 ZENSEN--DA SILVA                        
*/

--Question 16 : Quels sont les 10 premiers noms des etudiants ordonnes par ordre alphabetique ?
-- Al. Rel. EtudiantInfo{nomEtu ORDER BY ASC AND nomEtu <=10}[nomEtu]
SELECT nomEtu
FROM EtudiantInfo
WHERE rownum <= 10
ORDER BY nomEtu;

/*
10 lignes s�lectionn�es. 

NOMETU                                  
----------------------------------------
ALEXANDRE
ANNEIX
ARANDEL
BARATHON
BARIOU
...
*/

/*Question 17 : Proposez quelques requetes pour pratiquer les operations : projection+restriction,
union/intersection/diff�erence ensembliste, tri.*/

DROP TABLE EnseignantInfo;
DROP TABLE EtudiantInfo;