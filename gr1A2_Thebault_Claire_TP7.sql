/*
TP7 SQL :  Recapitulatif : Algebre relationnelle et Requetes SQL
Claire
Th?bault
GR1A2
*/
/*
EtudiantInfo(noEtu(1), nomEtu, prenom, groupe)
EnseignantInfo(noEns(1), nomEns, prenomEns)
*/

/*
Formulez en algebre relationnel et en SQL les interrogations suivantes, et donnez les
reponses. Attention : S?il y a plus de 5 tuples, vous devez donner le nombre de tuples et copier les 5
premiers tuples avec les nom des colonnes.
*/
/*
Q10 : Quels sont les noms des etudiants du groupe A qui ont le meme prenom qu?un enseignant ?
Al. rel. : EtudiantInfo[[UPPER(prenom) = UPPER(prenomEns)]]EnseignantInfo{groupe='A'}[UPPER(nomEtu)]
*/
SELECT DISTINCT UPPER(nomEtu)
FROM EtudiantInfo, EnseignantInfo
WHERE UPPER(prenom) = UPPER(prenomEns)
AND UPPER(groupe) ='A';
/*
UPPER(NOMETU)                           
----------------------------------------
PATINEC
*/

/*
Q11 : Quels sont les noms et prenoms des etudiants qui ont le meme prenom que l'enseignant identifie
par 3 ou 4 ?
Al. rel. : (EtudiantInfo[[prenom = prenomEns]]EnseignantInfo){noEns = (3 ou 4)}[nomEtu, prenom]
*/
SELECT DISTINCT UPPER(nomEns), UPPER(prenom)
FROM EnseignantInfo, EtudiantInfo
WHERE (noEns = 3 OR noEns = 4)
AND prenomEns = prenom;
/*
UPPER(NOMENS)        UPPER(PRENOM)                 
-------------------- ------------------------------
GODIN                THIBAULT  
*/

/*
Q12 : Quels sont les noms des enseignants qui ont le meme prenom que l'enseignant identifie par 5 ?
Al. rel. : EnseignantInfo E1[[E1.prenomEns = E2.prenomEns]] EnseignantInfo E2 {E1.noEns = 5 AND E2.noEns != 5}[nomEns]
*/
SELECT DISTINCT UPPER(E2.nomEns)
FROM EnseignantInfo E1, EnseignantInfo E2
WHERE UPPER(E1.prenomEns) = UPPER(E2.prenomEns)
AND E1.noEns = 5
AND E2.noEns != 5;
/*
UPPER(E2.NOMENS)    
--------------------
JOUCLA
*/

/*
Q13 : Quels sont les noms et prenoms des etudiants dont le prenom est porte par un autre ?
Al. rel. : EtudiantInfo E1[[UPPER(E1.prenom) = UPPER(E2.prenom) AND UPPER(E1.noEtu) != UPPER(E2.noEtu)]] EtudiantInfo E2 [UPPER(E1.nomEtu), UPPER(E1.prenom)]
*/
SELECT DISTINCT UPPER(E1.nomEtu), UPPER(E1.prenom)
FROM EtudiantInfo E1, EtudiantInfo E2
WHERE UPPER(E1.prenom) = UPPER(E2.prenom)
AND UPPER(E1.noEtu) != UPPER(E2.noEtu);
/*
UPPER(E1.NOMETU)                         UPPER(E1.PRENOM)              
---------------------------------------- ------------------------------
MORNET                                   QUENTIN                       
GUINOT                                   WILLIAM                       
GAUFFENY                                 PAUL                          
BELTRAME                                 MA�L                          
LEGEAY                                   QUENTIN                                           

20 lignes s�lectionn�es. 
*/

/*
Q14 : Quels sont les prenoms qui existent dans au moins 2 groupes ?
Al. rel. : EtudiantInfo E1[[UPPER(E1.prenom) = UPPER(E2.prenom) AND  UPPER(E1.groupe) != UPPER(E2.groupe)]] EtudiantInfo E2 [UPPER(E1.prenom)]
*/
SELECT DISTINCT UPPER(E1.prenom)
FROM EtudiantInfo E1, EtudiantInfo E2
WHERE UPPER(E1.prenom) = UPPER(E2.prenom)
AND UPPER(E1.groupe) != UPPER(E2.groupe);
/*
UPPER(E1.PRENOM)              
------------------------------
WILLIAM
BENJAMIN
SAMUEL
LOUIS
PAUL
QUENTIN
NATHAN
MA�L

8 lignes s�lectionn�es.
*/


/*
Formulez en SQL les interrogations suivantes (sans alg`ebre relationnelle), et donnez les
r?eponses.
*/
/*
Q15 : Quels sont les cinq premiers prenoms par ordre alphabetique portes par les etudiants ?
*/
SELECT *
FROM (
    SELECT DISTINCT UPPER(prenom)
    FROM EtudiantInfo
    ORDER BY UPPER(prenom)
    )
WHERE ROWNUM <=5;
/*
UPPER(PRENOM)                 
------------------------------
ABDUL-MALIK
ALEXANDRE
ALEXIA
ALEXIS
ALIAUME
*/

/*
Q16 : Quels sont les cinq derniers pr?enoms des ?etudiants ordonn?es par ordre alphab?etique des pr?enoms ?
*/
SELECT *
FROM (
    SELECT DISTINCT UPPER(prenom)
    FROM EtudiantInfo
    ORDER BY UPPER(prenom) DESC
    )
WHERE ROWNUM <=5;
/*
UPPER(PRENOM)                 
------------------------------
YANIS
WILLIAM
VICTOR
TITOUAN
THOMAS
*/

/*
Q17 : Quels sont les cinq premiers prenoms commencant par S et ordonnes par ordre alphabetique
portes par les etudiants ou les enseignants ?
*/
SELECT *
FROM (
    SELECT DISTINCT UPPER(prenom) AS leprenom
    FROM EtudiantInfo
    WHERE UPPER(prenom) LIKE 'S%'
    UNION
    SELECT DISTINCT UPPER(prenomEns) AS leprenom
    FROM EnseignantInfo
    WHERE UPPER(prenomEns) LIKE 'S%'
    ORDER BY leprenom
    )
WHERE ROWNUM <=5;
/*
LEPRENOM                      
------------------------------
SAMUEL
SARAH
SEBASTIEN
SERGE
SOPHIE
*/

/*
Q18 : Pour chaque enseignant identifie son num?ero, afficher les noms et pr?enoms des ?etudiants qui ont
le m?eme pr?enom que lui.
*/
SELECT noEns, UPPER(noEtu), UPPER(prenom)
FROM EnseignantInfo, EtudiantInfo
WHERE UPPER(prenom) = UPPER(prenomEns);

/*Ou
SELECT noEns, UPPER(nomEtu), UPPER(prenom)
FROM EnseignantInfo
JOIN EtudiantInfo ON UPPER(prenomEns) = UPPER(prenom);
/*
     NOENS UPPER(NOETU)                             UPPER(PRENOM)                 
---------- ---------------------------------------- ------------------------------
         8 81                                       FRANCOIS                      
         3 85                                       THIBAULT 
*/

/*
Q19 : Pour chaque enseignant identifi?e son num?ero, afficher les noms et pr?enoms des ?etudiants qui ont
le meme prenom que lui, s'il existe et rien sinon.
*/
SELECT noEns, UPPER(nomEtu), UPPER(prenom)
FROM EnseignantInfo, EtudiantInfo
WHERE UPPER(prenomEns) = UPPER(prenom(+));
/* Ou
SELECT noEns, UPPER(nomEtu), UPPER(prenom)
FROM EnseignantInfo
LEFT JOIN EtudiantInfo ON UPPER(prenomEns) = UPPER(prenom);
*/
/*
     NOENS UPPER(NOETU)                             UPPER(PRENOM)                 
---------- ---------------------------------------- ------------------------------
        10                                                                        
        15                                                                        
        16                                                                        
        12                                                                                                                                               

20 lignes s�lectionn�es.
*/

/*
Q20 : Proposez d?autres requ?etes de jointures et d?auto-jointures s?il vous reste le temps.
*/