/*
TD & TP3 -  R?ecapitulatif : Traduction, LDD (CREATE, ALTER, DROP),
LMD (INSERT, UPDATE, DELETE)
Nom : Th�bault
Pr?nom : Claire
Groupe : A2
*/
/*
Vehicule (immat (1), leModele = @Modele.idModele, dateAchat)
Client (idClient (1), nom (NN), prenom, adresse)
Modele (idModele (1), marque, couleur, puissance)
Location (unClient = @Client.idClient (1), unVehicule = @Vehicule.immat (1), dateLocation, duree)
*/

--Question 1 :
/*Question r�pondue pendant le TD*/

--Question 2 :

DROP TABLE Travail;
DROP TABLE Voiture;
DROP TABLE Constructeur;
DROP TABLE Concession;
DROP TABLE Client;


CREATE TABLE Client (
    idClient NUMBER
        CONSTRAINT pk_client PRIMARY KEY,
    nom VARCHAR2(30)
        CONSTRAINT nn_nom NOT NULL,
    prenom VARCHAR2(30),
    adresse VARCHAR2(30)
    )
;


CREATE TABLE Modele (
    idModele NUMBER
        CONSTRAINT pk_Modele PRIMARY KEY,
    marque VARCHAR2(30),
    couleur VARCHAR2(30),
    puissance VARCHAR2(30)
    )
;
    

CREATE TABLE Vehicule (
    immat VARCHAR2(30)
        CONSTRAINT pk_vehicule PRIMARY KEY,
    leModele NUMBER
        CONSTRAINT fk_modele_vehicule REFERENCES Modele(idModele),
    dateAchat DATE
    )
;
   
    
CREATE TABLE Location (
    unClient NUMBER
        CONSTRAINT fk_client_loc REFERENCES Client(idClient),
    unVehicule VARCHAR2(20)
        CONSTRAINT fk_vehicule_loc REFERENCES Vehicule(immat),
    dateLocation DATE,
    duree NUMBER
        CONSTRAINT ck_duree CHECK (duree>0)
    )
;
    

--Question 3 :
ALTER TABLE Vehicule 
    ADD CONSTRAINT ck_dateAchatNN CHECK (dateAchat IS NOT NULL);
    
--Question 4 : 

INSERT INTO Client VALUES(1, 'Garcia', 'Ellais', '20 rue de la R?publique');
INSERT INTO Modele VALUES(1, 'Honda', 'Gris', 5);
INSERT INTO Vehicule VALUES('AB-232-CD', 1, TO_DATE('24/10/2019', 'DD/MM/YYYY'));
INSERT INTO Location VALUES(1,'AB-232-CD', TO_DATE('30/11/2019', 'DD/MM/YYYY'),5);



--Question 6 :
/*
Concession(idConc(1), nomConc, capital)
Constructeur(idConst(1), nomConst(NN))
Client(idClient(1), nomClient(2), prenomClient(2))
Voiture(immat(1), modele(NN), couleur, laConcession=@Concession.idConc, leClient=@Client.idClient, leConstructeur=@Constructeur.idConst(NN))
Travail([unConstructeur=@Constructeur.idConst, unClient=@Client.idClient](1), dateContrat)

Contrainte textuelle :
- tout client doit acheter au moins une voiture
*/


--Question 6:
/*
Contraintes textuelles :
*/

DROP TABLE Location;
DROP TABLE Vehicule;
DROP TABLE Modele;
DROP TABLE Client;



CREATE TABLE Concession (
    idConc NUMBER
        CONSTRAINT pk_concession PRIMARY KEY,
    nomConc VARCHAR2(30),
    capital NUMBER
);


CREATE TABLE Constructeur (
    idConst NUMBER
        CONSTRAINT pk_constructeur PRIMARY KEY,
    nomConst VARCHAR2(30)
        CONSTRAINT nn_nomConst NOT NULL
);


CREATE TABLE Client (
    idClient NUMBER
        CONSTRAINT pk_client PRIMARY KEY,
    nomClient VARCHAR2(30),
    prenomClient VARCHAR2(30)
);


CREATE TABLE Voiture (
    immat NUMBER
        CONSTRAINT pk_voiture PRIMARY KEY,
    modele VARCHAR2(30)
        CONSTRAINT nn_modele NOT NULL,
    couleur VARCHAR2(30),
    laConcession NUMBER
        CONSTRAINT fk_concession_voiture REFERENCES Concession(idConc),
    leClient NUMBER
        CONSTRAINT fk_client_voiture REFERENCES Client(idClient),
    leConstructeur NUMBER
        CONSTRAINT fk_constructeur_voiture REFERENCES Constructeur(idConst)
        CONSTRAINT nn_leConstructeur NOT NULL
    )
;


CREATE TABLE Travail (
    unConstructeur NUMBER
        CONSTRAINT fk_constructeur_travail REFERENCES Constructeur(idConst),
    unClient NUMBER
        CONSTRAINT fk_client_travail REFERENCES Client(idClient),
    dateContrat DATE,
    CONSTRAINT pk_travail PRIMARY KEY (unConstructeur, unClient)
     
);


--Question 7 :
ALTER TABLE Travail
     ADD CONSTRAINT ck_dateContratNN CHECK (dateContrat IS NOT NULL);
     
--Question 8 :
ALTER TABLE Client ADD email VARCHAR2(30)
    CONSTRAINT ck_email CHECK(email LIKE '%_@%_.%_');
    
--Question 9 :

INSERT INTO Concession VALUES (1, 'concession1', 10000);
INSERT INTO Constructeur VALUES (1, 'renault');
INSERT INTO Client VALUES(1, 'Ferron', 'Sarah','sarah@gmail.fr');
INSERT INTO Client VALUES(2, 'Garcia', 'Ellais','ellais@gmail.fr');

INSERT INTO Voiture VALUES (1,'twingo','rouge',1, 1, 1);
INSERT INTO Voiture VALUES (2,'scenic','noire',1, 2, 1);


-- Question 10 :
-- a) l'existence de la cl�  primaire de la table Concession
INSERT INTO Concession(idConc) VALUES(NULL);
/*
INSERT INTO Concession(idConc) VALUES(NULL)
Rapport d'erreur -
ORA-01400: cannot insert NULL into ("CLAIRE"."CONCESSION"."IDCONC")
*/

-- b) l'unicit� de la cl� primaire de la table Concession
INSERT INTO Concession(idConc) VALUES(2);
INSERT INTO Concession VALUES(2, 'concession2', 10000);
/*
INSERT INTO Concession VALUES(2, 'concession2', 10000)
Rapport d'erreur -
ORA-00001: unique constraint (CLAIRE.PK_CONCESSION) violated
*/

-- c) l'existence de la cl� �trang�re Concession dans la table Voiture
INSERT INTO Voiture VALUES(1,'twingo','rouge',NULL, 1, 1);
/*
INSERT INTO Voiture VALUES(1,'twingo','rouge',NULL, 1, 1)
Rapport d'erreur -
ORA-00001: unique constraint (CLAIRE.PK_VOITURE) violated
*/


-- d) mettre � jour avec UPDATE
UPDATE Concession 
    SET nomConc = 'Concession Renault'
    WHERE idConc = 1;
    
--e) Enlever avec DELETE FROM
INSERT INTO Concession VALUES (3, 'inexistant', 10);
DELETE FROM concession
WHERE nomConc = 'inexistant';